package com.dmityinc.design.theme

import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Shapes

val Shapes = Shapes(
    small = RoundedCornerShape(dp4),
    medium = RoundedCornerShape(dp4),
    large = RoundedCornerShape(dp0)
)
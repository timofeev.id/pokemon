package com.dmityinc.design.theme

import androidx.compose.ui.graphics.Color

val Black = Color(0xFF404357)
val White = Color(0xFFFFFFFF)

val Gray100 = Color(0xFFF4F5FB)
val Gray200 = Color(0xFFE2E3E9)
val Gray300 = Color(0xFFC4C6D4)
val Gray400 = Color(0xFF898DA8)
val Gray500 = Color(0xFF6B7192)
val Gray600 = Color(0xFF565A75)

val Red100 = Color(0xFFFFE9F0)
val Red300 = Color(0xFFFF497D)
val Red500 = Color(0xFF820B3D)

val Yellow100 = Color(0xFFFDF0D9)
val Yellow300 = Color(0xFFF5B242)
val Yellow500 = Color(0xFF945F08)

val Green100 = Color(0xFFCFFFF2)
val Green300 = Color(0xFF00BA88)
val Green500 = Color(0xFF005D44)

val Orange100 = Color(0xFFFFE3DC)
val Orange300 = Color(0xFFFF7F5C)
val Orange500 = Color(0xFF5D1400)

val Blue100 = Color(0xFFD9E7FD)
val Blue300 = Color(0xFF4285F5)
val Blue500 = Color(0xFF083C94)


val TitleActive = Black
val Placeholder = Gray400
val Background = Gray100

package com.dmityinc.design.theme

import androidx.compose.ui.unit.dp

val dp0 = 0.dp
val dp4 = 4.dp
val dp8 = 8.dp
val dp12 = 12.dp
val dp16 = 16.dp
val dp20 = 20.dp
val dp24 = 24.dp
val dp32 = 32.dp
val dp40 = 40.dp
val dp44 = 44.dp
val dp48 = 48.dp
val dp56 = 56.dp
val dp64 = 64.dp
val dp80 = 80.dp
val dp96 = 96.dp
val dp128 = 128.dp
val dp160 = 160.dp
val dp192 = 192.dp
val dp224 = 224.dp
val dp256 = 256.dp
val dp320 = 320.dp

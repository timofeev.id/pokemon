package com.dmityinc.design.theme

import androidx.compose.foundation.isSystemInDarkTheme
import androidx.compose.material.MaterialTheme
import androidx.compose.material.darkColors
import androidx.compose.material.lightColors
import androidx.compose.runtime.Composable

private val LightColorPalette = lightColors(
    primary = Blue300,
    primaryVariant = Blue500,
    onPrimary = Background,
    secondary = Orange300,
    secondaryVariant = Orange500,
    onSecondary = Black,
    surface = White,
    error = Red300,
    onError = White,
    background = Gray200,
    onBackground = Black,
    onSurface = Black
)

private val DarkColorPalette = darkColors(
    primary = Blue300,
    primaryVariant = Blue500,
    onPrimary = Black,
    secondary = Orange300,
    secondaryVariant = Orange500,
    onSecondary = White,
    surface = Black,
    error = Red300,
    onError = Black,
    background = Black,
    onBackground = White,
    onSurface = White
)

@Composable
fun AppTheme(
    darkTheme: Boolean = isSystemInDarkTheme(),
    content: @Composable () -> Unit
) {
    val colors = if (darkTheme) {
        DarkColorPalette
    } else {
        LightColorPalette
    }

    MaterialTheme(
        colors = colors,
        typography = Typography,
        shapes = Shapes,
        content = content
    )
}

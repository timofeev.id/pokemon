package com.dmityinc.design.composables

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.RowScope
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material.IconButton
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.material.TopAppBar
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.ColorFilter
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.tooling.preview.Preview
import com.dmityinc.design.R
import com.dmityinc.design.theme.dp0
import com.dmityinc.design.theme.dp16
import com.dmityinc.design.theme.toBold

@Composable
fun TopBarWithMenu(
    modifier: Modifier = Modifier,
    title: String,
    titleColor: Color = Color.Unspecified,
    style: TextStyle = MaterialTheme.typography.h3.toBold(),
    actions: @Composable RowScope.() -> Unit = {},
    onIconClick: () -> Unit
) {
    TopAppBar(
        modifier = modifier
            .fillMaxWidth()
            .padding(end = dp16),
        title = {
            Text(
                text = title,
                style = style,
                color = titleColor,
                textAlign = TextAlign.Center,
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(end = dp16),
                overflow = TextOverflow.Ellipsis,
                maxLines = 1
            )
        },
        backgroundColor = Color.Transparent,
        elevation = dp0,
        navigationIcon = {
            IconButton(onClick = onIconClick) {
                Image(
                    painter = painterResource(R.drawable.ic_arrow_back),
                    contentDescription = null,
                    colorFilter = ColorFilter.tint(MaterialTheme.colors.onPrimary)
                )
            }
        },
        actions = actions
    )
}

@Composable
@Preview
private fun TopBarPreview() {
    TopBarWithMenu(
        modifier = Modifier.fillMaxWidth(),
        title = stringResource(id = R.string.retry),
        onIconClick = {
            // TODO transfer back
        }
    )
}

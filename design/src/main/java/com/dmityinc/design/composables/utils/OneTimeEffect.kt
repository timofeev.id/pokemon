package com.dmityinc.design.composables.utils

import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import kotlinx.coroutines.CoroutineScope

@Composable
fun OneTimeEffect(block: suspend CoroutineScope.() -> Unit) {
    LaunchedEffect(
        key1 = true,
        block = block
    )
}

package com.dmityinc.design.composables

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.height
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import com.dmityinc.design.theme.dp8
import com.dmityinc.design.R as design

@Composable
fun FullScreenError(
    modifier: Modifier,
    msg: String,
    showRetryBtn: Boolean = false,
    onRetryClick: () -> Unit = {}
) {
    Column(
        modifier = modifier.fillMaxSize(),
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        Text(
            text = msg,
            textAlign = TextAlign.Center,
            style = MaterialTheme.typography.body1
        )
        if (showRetryBtn) {
            Spacer(modifier = Modifier.height(dp8))
            PrimaryButton(
                text = stringResource(id = design.string.retry),
                onClick = onRetryClick
            )
        }
    }
}

package com.dmityinc.design.composables

import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.ButtonDefaults
import androidx.compose.material.LocalTextStyle
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.material.TextButton
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.TextStyle
import com.dmityinc.design.theme.dp12
import com.dmityinc.design.theme.dp16

@Composable
fun PrimaryButton(
    text: String,
    onClick: () -> Unit,
    modifier: Modifier = Modifier,
    enabled: Boolean = true,
    style: TextStyle = LocalTextStyle.current,
    textColor: Color = MaterialTheme.colors.onPrimary,
    buttonColor: Color = MaterialTheme.colors.primary
) {
    TextButton(
        modifier = modifier,
        shape = RoundedCornerShape(dp16),
        onClick = onClick,
        enabled = enabled,
        colors = ButtonDefaults.textButtonColors(
            backgroundColor = buttonColor
        ),
        contentPadding = PaddingValues(dp12)
    ) {
        Text(
            text = text,
            style = style,
            color = textColor
        )
    }
}

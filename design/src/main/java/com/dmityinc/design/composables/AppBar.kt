package com.dmityinc.design.composables

import androidx.annotation.DrawableRes
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.wrapContentSize
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Icon
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import com.dmityinc.design.R
import com.dmityinc.design.theme.dp16

@Composable
fun AppBar(
    tint: Color = Color.Unspecified,
    title: String = "",
    titleColor: Color = Color.Unspecified,
    @DrawableRes menuIcon: Int? = null,
    onIconClick: () -> Unit = {},
    onMenuClick: () -> Unit = {}
) {
    TopBarWithMenu(
        modifier = Modifier,
        title = title,
        titleColor = titleColor,
        actions = {
            menuIcon?.let { icon ->
                Icon(
                    modifier = Modifier
                        .wrapContentSize()
                        .clip(shape = RoundedCornerShape(dp16))
                        .clickable { onMenuClick.invoke() },
                    painter = painterResource(icon),
                    contentDescription = null,
                    tint = tint
                )
            }
        },
        onIconClick = onIconClick
    )
}

@Preview
@Composable
private fun Preview() {
    AppBar(
        title = stringResource(id = R.string.retry),
        titleColor = Color.Red,
        menuIcon = R.drawable.ic_baseline_star_border,
        onIconClick = {}
    ) {}
}

import org.jetbrains.kotlin.gradle.tasks.KotlinCompile
plugins {
    id("com.android.library")
    kotlin("android")
    kotlin("kapt")
}

apply {
    from("$rootDir/config/common-feature-module.gradle")
    from("$rootDir/config/compose-feature-module.gradle")
}

tasks.withType<KotlinCompile> {
    kotlinOptions.freeCompilerArgs += "-opt-in=kotlin.RequiresOptIn"
}

dependencies {
    implementation(libs.google.material)
    implementation(libs.bundles.accompanist)
}

package com.dmityinc.core.model.domain

import androidx.annotation.StringRes
import com.dmityinc.core.R
import com.google.gson.annotations.SerializedName

/**
 * All possible errors inside response
 *
 * @param messageToUserRes String resource with message to user
 */
sealed class ErrorEntity(
    @StringRes val messageToUserRes: Int? = null
) : Throwable() {

    /**
     * Bad request error class
     * @param status        Error response status code.
     * @param title         Error title.
     * @param type          Error type.
     */
    data class BadRequestError(
        @SerializedName("status")
        val status: Int? = null,
        @SerializedName("title")
        val title: String? = null,
        @SerializedName("type")
        val type: String? = null,
    ) : ErrorEntity()

    /**
     * Forbidden request error class
     * @param detail        Error detail
     * @param instance      ?
     * @param status        Error response status code.
     * @param title         Error title.
     * @param type          Error type.
     */
    data class Forbidden(
        @SerializedName("detail")
        val detail: String? = null,
        @SerializedName("instance")
        val instance: String? = null,
        @SerializedName("status")
        val status: Int? = null,
        @SerializedName("title")
        val title: String? = null,
        @SerializedName("type")
        val type: String? = null
    ) : ErrorEntity()

    /**
     * Error witch i don't expected at all
     * @param errorMsg  message to user
     * @param errorCode code number of error
     */
    class UnknownError(
        val errorCode: Int = 0,
        val errorMsg: String = ""
    ) : ErrorEntity(R.string.error_unknown)

    /**
     *
     * Server Error. Something went wrong on our side.
     * @param errorMsg  message to user
     * @param errorCode code number of error
     */
    class ServerError(
        val errorCode: Int = 0,
        val errorMsg: String = ""
    ) : ErrorEntity(R.string.error_server)

    /**
     * Error witch only user message
     */
    class UiError(
        messageToUserRes: Int
    ) : ErrorEntity(messageToUserRes)
}

package com.dmityinc.core.model.data

import com.dmityinc.core.model.domain.ErrorEntity
import timber.log.Timber

/** Global network response */
sealed class Response<T> {

    /**
     * Representation of network success
     * @param data result data
     */
    class Success<T>(
        val data: T
    ) : Response<T>()

    /**
     * Representation of network error
     * @param error error class
     */
    class Error<T>(
        val error: ErrorEntity
    ) : Response<T>()
}

/**
 * Fun to get result if success or null if not
 * @return [T] if success, null if not
 */
fun <T> Response<T>.getResult() = if (this is Response.Success<T>) {
    this.data
} else {
    null
}


/**
 * Fun to check if success
 * @param block block of code to invoke then Success result
 */
inline fun <T> Response<T>.doOnSuccess(block: (T) -> Unit): Response<T> {
    if (this is Response.Success) {
        block.invoke(this.data)
    }
    return this
}

/**
 * Fun to check if error
 * @param block block of code to invokr then Error result
 */
inline fun <T> Response<T>.doOnError(block: (ErrorEntity) -> Unit): Response<T> {
    if (this is Response.Error) {
        block.invoke(this.error)
    }
    return this
}

/**
 * Fun to start when something done
 * @param block block of code to invoke on any result
 */
inline fun <T> Response<T>.doOnComplete(block: () -> Unit): Response<T> {
    block.invoke()
    return this
}

/** Run request with catch error and wrap to Response */
inline fun <T> doWithCatching(
    doWork: () -> T,
    errorMapper: (Throwable) -> ErrorEntity
): Response<T> {
    return try {
        Response.Success(doWork.invoke())
    } catch (e: Throwable) {
        Timber.e(e, "Response error")
        Response.Error(errorMapper.invoke(e))
    }
}

package com.dmityinc.core.model.presentation

import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import com.dmityinc.core.model.domain.ErrorEntity

open class BaseViewState {
    val error: MutableState<ErrorEntity?> = mutableStateOf(null)
    val isLoading: MutableState<Boolean> = mutableStateOf(true)
}

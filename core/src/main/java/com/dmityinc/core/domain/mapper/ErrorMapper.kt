package com.dmityinc.core.domain.mapper

import com.dmityinc.core.model.domain.ErrorEntity
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import retrofit2.HttpException
import timber.log.Timber
import java.io.IOException

/**
 * Error mapper to user friendly error
 *
 * @param gson Gson object
 */
class ErrorMapper (
    private val gson: Gson
) : Mapper<Throwable, ErrorEntity> {

    override fun map(from: Throwable): ErrorEntity {
        return when (from) {
            is HttpException -> mapHttpException(from)
            else -> ErrorEntity.UnknownError(errorMsg = from.message.orEmpty())
        }
    }

    private fun mapHttpException(e: HttpException) = when (e.code()) {
        400 -> parseBadRequest(e)
        403 -> parseForbidden(e)
        500 -> ErrorEntity.ServerError(
            errorCode = e.code(),
            errorMsg = e.message()
        )
        else -> ErrorEntity.UnknownError(
            errorCode = e.code(),
            errorMsg = e.message()
        )
    }

    private fun parseForbidden(e: HttpException): ErrorEntity = try {
        val body = e.response()?.errorBody()?.toString()
        val adapter = gson.getAdapter(ErrorEntity.Forbidden::class.java)
        val data = adapter.fromJson(body)
        Timber.d("Error = ${data.title} type = ${data.type}")
        data
    } catch (ioEx: IOException) {
        Timber.d(ioEx,"Error during parsing")
        ErrorEntity.UnknownError(
            errorCode = e.code(),
            errorMsg = e.message()
        )
    }

    private fun parseBadRequest(e: HttpException): ErrorEntity = try {
        val type = object : TypeToken<ErrorEntity.BadRequestError>() {}.type
        val data: ErrorEntity.BadRequestError = gson.fromJson(e.response()?.errorBody()?.charStream(), type)
        data
    } catch (ex: Exception) {
        Timber.e(ex,"Error during parsing")
        ErrorEntity.UnknownError(
            errorCode = e.code(),
            errorMsg = e.message()
        )
    }
}

package com.dmityinc.core.domain

/**
 * Base interface of UseCase
 */
interface UseCase<REQUEST, RESPONSE>{

    suspend fun doWork(data: REQUEST): RESPONSE
}

package com.dmityinc.core.domain.mapper

/**
 * Convert from {@link T} to {@link R}.
 *
 * @param <T> from
 * @param <R> to
 */
interface Mapper<T, R> {

    /** {@link T} -> {@link R} */
    fun map(from: T): R

    /** {@link List<T>} -> {@link List<R>} */
    fun mapList(fromList: List<T>): List<R> = fromList.map(::map)

    /** {@link R} -> {@link T} */
    fun reverse(from: R): T = throw IllegalArgumentException("Mapper not support reverse")
}
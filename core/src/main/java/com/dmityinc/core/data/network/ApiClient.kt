package com.dmityinc.core.data.network

import com.dmityinc.core.BuildConfig
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.Response
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import timber.log.Timber
import java.util.concurrent.TimeUnit

/**
 * Network client Api functions
 */
object ApiClient {

    private const val CONNECTION_TIMEOUT: Long = 180L
    private const val READ_TIMEOUT: Long = 180L
    private const val WRITE_TIMEOUT: Long = 180L

    /** Default implementation of gson */
    val gson: Gson = GsonBuilder()
        .setLenient()
        .create()

    /** Create retrofit client with [T] class */
    fun <T> createApiClient(
        classT: Class<T>,
        apiUrl: String
    ): T = Retrofit
        .Builder()
        .apply {
            baseUrl(apiUrl)
            client(okHttpClient)
            addConverterFactory(GsonConverterFactory.create(gson))
        }
        .build()
        .create(classT)

    /** Create default okhttp client */
    private val okHttpClient: OkHttpClient
        get() = OkHttpClient()
            .newBuilder()
            .connectTimeout(CONNECTION_TIMEOUT, TimeUnit.SECONDS)
            .readTimeout(READ_TIMEOUT, TimeUnit.SECONDS)
            .writeTimeout(WRITE_TIMEOUT, TimeUnit.SECONDS)
            .addInterceptor(errorInterceptor)
            .run {
                if (BuildConfig.DEBUG) {
                    addInterceptor(HttpLoggingInterceptor().apply {
                        level = HttpLoggingInterceptor.Level.BODY
                    })
                }
                this
            }
            .build()

    /** Default interceptor with logging all HTTP errors */
    private val errorInterceptor = Interceptor { chain ->
        val request: Request = chain.request()
        val response: Response = chain.proceed(request)
        val statusCode = response.code.toString()
        val isErrorCode = statusCode.startsWith("4") || statusCode.startsWith("5")
        if (BuildConfig.DEBUG && isErrorCode) {
            Timber.e("errorInterceptor HTTP ERROR $statusCode $response")
        }
        response
    }
}
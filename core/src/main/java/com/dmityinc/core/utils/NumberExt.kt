package com.dmityinc.core.utils

import java.text.DecimalFormat

fun Double?.format(): String? {
    val df = DecimalFormat("0.#")
    return df.format(this)
}

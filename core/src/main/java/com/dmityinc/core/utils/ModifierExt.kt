package com.dmityinc.core.utils

import androidx.compose.foundation.background
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.graphics.Color

fun Modifier.radialGradient(colors: List<Color>): Modifier {
    return this.then(
        background(
            brush = Brush.radialGradient(
                colors = colors
            )
        )
    )
}

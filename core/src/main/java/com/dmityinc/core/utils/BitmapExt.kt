package com.dmityinc.core.utils

import android.graphics.Bitmap
import android.graphics.BlurMaskFilter
import android.graphics.BlurMaskFilter.Blur
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Matrix
import android.graphics.Matrix.ScaleToFit
import android.graphics.Paint
import android.graphics.PorterDuff
import android.graphics.PorterDuffXfermode
import android.graphics.RectF


fun Bitmap.addShadow(
    dstHeight: Int,
    dstWidth: Int,
    color: Int = Color.BLACK,
    size: Int = 30,
    dx: Float = 20f,
    dy: Float = 20f
): Bitmap {
    val mask = Bitmap.createBitmap(dstWidth, dstHeight, Bitmap.Config.ALPHA_8)
    val scaleToFit = Matrix()
    val src = RectF(0f, 0f, width.toFloat(), height.toFloat())
    val dst = RectF(0f, 0f, dstWidth - dx, dstHeight - dy)
    scaleToFit.setRectToRect(src, dst, ScaleToFit.CENTER)
    val dropShadow = Matrix(scaleToFit)
    dropShadow.postTranslate(dx, dy)
    val maskCanvas = Canvas(mask)
    val paint = Paint(Paint.ANTI_ALIAS_FLAG)
    maskCanvas.drawBitmap(this, scaleToFit, paint)
    paint.xfermode = PorterDuffXfermode(PorterDuff.Mode.SRC_OUT)
    maskCanvas.drawBitmap(this, dropShadow, paint)
    paint.apply {
        reset()
        isAntiAlias = true
        this.color = color
        maskFilter = BlurMaskFilter(size.toFloat(), Blur.NORMAL)
        isFilterBitmap = true
    }
    val ret = Bitmap.createBitmap(dstWidth, dstHeight, Bitmap.Config.ARGB_8888)
    val retCanvas = Canvas(ret)
    retCanvas.apply {
        drawBitmap(mask, 0f, 0f, paint)
        drawBitmap(this@addShadow, scaleToFit, null)
    }
    mask.recycle()
    return ret
}
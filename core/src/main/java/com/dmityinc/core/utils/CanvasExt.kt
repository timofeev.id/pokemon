package com.dmityinc.core.utils

import androidx.compose.runtime.Composable
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.TextUnit

@Composable
fun Dp.toPx(): Float {
    return LocalDensity
        .current
        .run { this@toPx.toPx() }
}

@Composable
fun TextUnit.toPx(): Float {
    return LocalDensity
        .current
        .run { this@toPx.toPx() }
}
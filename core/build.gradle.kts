plugins {
    id("com.android.library")
    kotlin("android")
    kotlin("kapt")
}

apply {
    from("$rootDir/config/common-feature-module.gradle")
    from("$rootDir/config/compose-feature-module.gradle")
}

dependencies {
    implementation(libs.gson)
    implementation(libs.bundles.retrofit)
    implementation(libs.okhttp3.logging)
    implementation(libs.androidx.annotation)
}

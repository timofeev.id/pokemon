package com.dmityinc.navigation.impl.model

import androidx.compose.ui.graphics.vector.ImageVector

class BottomNavItem(
    val label: String,
    val icon: ImageVector,
    val route: String
)

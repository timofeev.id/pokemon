package com.dmityinc.navigation.impl

import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.runtime.Composable
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import com.dmityinc.feature.pockemoninfo.presentation.PokemonInfoScreen
import com.dmityinc.feature.pockemoninfo.presentation.PokemonInfoViewModel
import com.dmityinc.feature.pockemonslist.presentation.PokemonListScreen
import com.dmityinc.feature.pockemonslist.presentation.PokemonListViewModel
import com.dmityinc.navigation.api.NavigationRoutes
import com.dmityinc.navigation.api.NavigationRoutes.PockemonFeature.PokemonInfo

@Composable
fun NavHostContainer(
    navController: NavHostController,
    padding: PaddingValues
) {
    NavHost(
        navController = navController,
        startDestination = NavigationRoutes.PockemonFeature.POKEMON_LIST,
        builder = {

            composable(NavigationRoutes.BottomNav.POKEMONS) {
                // TODO Add View
            }

            composable(NavigationRoutes.BottomNav.FAVORITE) {
                // TODO Add View
            }

            composable(NavigationRoutes.BottomNav.SETTINGS) {
                // TODO Add View
            }

            composable(PokemonInfo.PATH) {
                val pokemonName = it.arguments?.getString(PokemonInfo.NAME)
                val viewModel = hiltViewModel<PokemonInfoViewModel>().also {
                    it.pokemonName = pokemonName.orEmpty()
                }
                PokemonInfoScreen(
                    viewModel = viewModel,
                    navController = navController,
                    paddingValues = padding
                )
            }

            composable(NavigationRoutes.PockemonFeature.POKEMON_LIST) {
                val viewModel = hiltViewModel<PokemonListViewModel>()
                PokemonListScreen(
                    viewModel = viewModel,
                    navController = navController,
                    paddingValues = padding
                )
            }
        }
    )
}

package com.dmityinc.navigation.impl

import androidx.compose.material.BottomNavigation
import androidx.compose.material.BottomNavigationItem
import androidx.compose.material.Icon
import androidx.compose.material.Text
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Favorite
import androidx.compose.material.icons.filled.Home
import androidx.compose.material.icons.filled.Settings
import androidx.compose.runtime.Composable
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.navigation.NavController
import androidx.navigation.compose.currentBackStackEntryAsState
import androidx.navigation.compose.rememberNavController
import com.dmityinc.navigation.api.NavigationRoutes
import com.dmityinc.navigation.impl.model.BottomNavItem

@Composable
fun BottomNavigationBar(navController: NavController) {

    val bottomNavItems = listOf(
        BottomNavItem(
            label = stringResource(id = R.string.pockemons),
            icon = Icons.Filled.Home,
            route = NavigationRoutes.BottomNav.POKEMONS
        ),

        BottomNavItem(
            label = stringResource(id = R.string.favorite),
            icon = Icons.Filled.Favorite,
            route = NavigationRoutes.BottomNav.FAVORITE
        ),

        BottomNavItem(
            label = stringResource(id = R.string.settings),
            icon = Icons.Filled.Settings,
            route = NavigationRoutes.BottomNav.SETTINGS
        )
    )

    BottomNavigation(
        backgroundColor = Color.White,
    ) {
        val navBackStackEntry = navController.currentBackStackEntryAsState()
        val currentRoute = navBackStackEntry.value?.destination?.route
        bottomNavItems.forEach { navItem ->
            BottomNavigationItem(
                selected = currentRoute == navItem.route,
                onClick = {
                    navController.navigate(navItem.route)
                },
                icon = {
                    Icon(imageVector = navItem.icon, contentDescription = navItem.label)
                },
                label = {
                    Text(text = navItem.label)
                }
            )
        }
    }
}

@Preview
@Composable
private fun PreviewBottomNavigationBar() {
    val navController = rememberNavController()
    BottomNavigationBar(navController = navController)
}

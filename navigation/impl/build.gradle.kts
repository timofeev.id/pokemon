plugins {
    id("com.android.library")
    kotlin("android")
    kotlin("kapt")
    id("dagger.hilt.android.plugin")
}

apply {
    from("$rootDir/config/common-hilt.gradle")
    from("$rootDir/config/common-feature-module.gradle")
    from("$rootDir/config/compose-feature-module.gradle")
}

dependencies {
    implementation(project(":core"))
    implementation(project(":design"))
    implementation(project(":feature:pokemonslist"))
    implementation(project(":feature:pokemoninfo"))
    implementation(project(":navigation:api"))

    implementation(libs.google.material)
}
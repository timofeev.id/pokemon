plugins {
    id("com.android.library")
    kotlin("android")
    kotlin("kapt")
}

apply {
    from("$rootDir/config/common-feature-module.gradle")
}

dependencies {
    implementation(libs.google.material)
}
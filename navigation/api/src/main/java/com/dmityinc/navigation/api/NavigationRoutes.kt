package com.dmityinc.navigation.api

object NavigationRoutes {

    object BottomNav {
        const val POKEMONS = "pokemons"
        const val FAVORITE = "favorite"
        const val SETTINGS = "settings"
    }

    object PockemonFeature {
        const val POKEMON_LIST = "pokemonList"

        object PokemonInfo {
            const val NAME = "pokemonName"
            const val PATH = "pokemonInfo/{$NAME}"
            fun pokemonInfoScreenLink(pokemonName: String) = "pokemonInfo/$pokemonName"
        }
    }
}

package com.dmityinc.pokemon

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.material.Scaffold
import androidx.compose.runtime.Composable
import androidx.compose.ui.tooling.preview.Preview
import androidx.core.view.WindowCompat
import androidx.navigation.compose.rememberNavController
import com.dmityinc.design.theme.AppTheme
import com.dmityinc.navigation.impl.NavHostContainer
import com.google.accompanist.insets.LocalWindowInsets
import com.google.accompanist.insets.ProvideWindowInsets
import com.google.accompanist.insets.rememberInsetsPaddingValues
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : ComponentActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        // Turn off the decor fitting system windows, which means we need to through handling insets
        WindowCompat.setDecorFitsSystemWindows(window, false)
        setContent {
            AppTheme {
                ProvideWindowInsets {
                    Navigation()
                }
            }
        }
    }

    @Composable
    fun Navigation() {
        val navController = rememberNavController()
        val contentPadding = rememberInsetsPaddingValues(
            insets = LocalWindowInsets.current.systemBars
        )
        // TODO Activate after done favorites and set content padding
        Scaffold(
//            modifier = Modifier
//                .padding(contentPadding)
//                .imePadding(),
            bottomBar = {
//                BottomNavigationBar(navController = navController)
            },
            content = { padding ->
                NavHostContainer(navController = navController, padding = contentPadding)
            }
        )
    }

    @Preview
    @Composable
    private fun PreviewNavigationExample() {
        Navigation()
    }

}

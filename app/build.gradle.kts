plugins {
    id("com.android.application")
    kotlin("android")
    kotlin("kapt")
    id("dagger.hilt.android.plugin")
}

apply {
    from("$rootDir/config/common-feature-module.gradle")
    from("$rootDir/config/compose-feature-module.gradle")
    from("$rootDir/config/common-hilt.gradle")
}

android {

    defaultConfig {
        applicationId = "com.dmityinc.pokemon"
    }

    buildTypes {
        getByName("release") {
            isMinifyEnabled = false
            proguardFiles(
                getDefaultProguardFile("proguard-android-optimize.txt"),
                "proguard-rules.pro"
            )
        }
    }
}

dependencies {
    implementation(kotlin("stdlib"))
    implementation(project(":core"))
    implementation(project(":design"))
    implementation(project(":feature:pokemonslist"))
    implementation(project(":feature:pokemoninfo"))
    implementation(project(":navigation:api"))
    implementation(project(":navigation:impl"))

    implementation(libs.google.material)
    implementation(libs.androidx.activity.compose)
    implementation(libs.androidx.core.ktx)
    implementation(libs.androidx.fragment.ktx)
    implementation(libs.bundles.accompanist)
    implementation(libs.androidx.compose.navigation)
}

buildscript {

    repositories {
        google()
        mavenCentral()
    }

    dependencies {
        classpath("com.android.tools.build:gradle:${libs.versions.gradlePluginVersion.get()}")
        classpath("com.google.dagger:hilt-android-gradle-plugin:${libs.versions.daggerVersion.get()}")
        classpath(kotlin("gradle-plugin", version = libs.versions.kotlin.get()))
    }
}

tasks.register("clean", Delete::class) {
    delete(rootProject.buildDir)
}

extra.apply {
    set("minSdkVersion", libs.versions.minSdk.get().toInt())
    set("targetSdkVersion", libs.versions.targetSdk.get().toInt())
    set("compileSdkVersion", libs.versions.compileSdk.get().toInt())
    set("composeVersion", libs.versions.compose.get())
}
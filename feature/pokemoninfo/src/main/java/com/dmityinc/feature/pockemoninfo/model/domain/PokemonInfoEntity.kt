package com.dmityinc.feature.pockemoninfo.model.domain

import com.dmityinc.feature.pokemoncommon.model.domain.PokemonType

data class PokemonInfoEntity (
    val name: String,
    val height: String?,
    val weight: String?,
    val hp: Double,
    val strength: Double,
    val agility: Double,
    val attack: Double,
    val imgUrlList: List<String>,
    val genTypes: List<PokemonType>,
    val isFavorite: Boolean
)

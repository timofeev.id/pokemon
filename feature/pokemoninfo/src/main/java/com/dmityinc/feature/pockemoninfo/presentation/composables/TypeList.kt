package com.dmityinc.feature.pockemoninfo.presentation.composables


import android.graphics.Color
import android.graphics.Paint
import androidx.compose.foundation.Canvas
import androidx.compose.foundation.layout.padding
import androidx.compose.material.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.drawscope.drawIntoCanvas
import androidx.compose.ui.graphics.nativeCanvas
import com.dmityinc.core.utils.toPx
import com.dmityinc.design.theme.dp12
import com.dmityinc.design.theme.dp24
import com.dmityinc.design.theme.dp8
import com.dmityinc.feature.pokemoncommon.model.domain.PokemonType

@Composable
internal fun TypeList(types: List<PokemonType>) {
    val dp12InPx = dp12.toPx()
    val dp24InPx = dp24.toPx()
    val dp8InPx = dp8.toPx()
    val paint = Paint()
        .apply {
            color = Color.WHITE
            textSize = MaterialTheme.typography.caption.fontSize.toPx()
        }
    types.forEach { type ->
        Canvas(modifier = Modifier.padding(top = dp12)) {
            drawCircle(
                color = type.color,
                radius = dp12InPx
            )
            drawIntoCanvas {
                val textWidth = paint.measureText(type.name)
                it.nativeCanvas.drawText(type.name, -(textWidth / 2), dp24InPx + dp8InPx, paint)
            }
        }
    }
}

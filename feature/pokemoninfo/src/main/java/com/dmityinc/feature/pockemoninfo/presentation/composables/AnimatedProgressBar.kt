package com.dmityinc.feature.pockemoninfo.presentation.composables

import androidx.compose.animation.core.animateFloatAsState
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.LinearProgressIndicator
import androidx.compose.material.MaterialTheme
import androidx.compose.material.ProgressIndicatorDefaults
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.style.TextAlign
import com.dmityinc.design.theme.Gray300
import com.dmityinc.design.theme.dp12
import com.dmityinc.design.theme.dp20
import kotlinx.coroutines.delay

@Composable
fun AnimatedProgressBar(
    modifier: Modifier = Modifier.fillMaxWidth(),
    activeColor: Color = MaterialTheme.colors.primary,
    maxValue: Double = 100.0,
    currentValue: Double = 0.0,
    title: String
) {
    val progress = remember { mutableStateOf(0f) }
    val animatedProgress by animateFloatAsState(
        targetValue = progress.value.toFloat(),
        animationSpec = ProgressIndicatorDefaults.ProgressAnimationSpec
    )
    Row(
        modifier = modifier,
        horizontalArrangement = Arrangement.Start,
        verticalAlignment = Alignment.CenterVertically
    ) {
        Text(
            modifier = Modifier.weight(0.2f),
            text = title,
            style = MaterialTheme.typography.body1,
            textAlign = TextAlign.Start,
            color = MaterialTheme.colors.surface
        )
        LinearProgressIndicator(
            modifier = Modifier
                .weight(0.8f)
                .height(dp12)
                .clip(RoundedCornerShape(dp20)),
            progress = animatedProgress,
            color = activeColor,
            backgroundColor = Gray300
        )
        LaunchedEffect(currentValue) {
            delay(500)
            progress.value = (currentValue / maxValue).toFloat()
        }
    }
}
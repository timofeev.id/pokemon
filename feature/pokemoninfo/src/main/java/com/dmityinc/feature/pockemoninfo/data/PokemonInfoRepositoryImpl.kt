package com.dmityinc.feature.pockemoninfo.data

import com.dmityinc.core.domain.mapper.Mapper
import com.dmityinc.feature.pockemoninfo.domain.PokemonInfoRepository
import com.dmityinc.feature.pockemoninfo.model.domain.PokemonInfoEntity
import com.dmityinc.feature.pokemoncommon.data.PokemonApi
import com.dmityinc.feature.pokemoncommon.model.data.pokemoninfo.PokemonInfoResponse

class PokemonInfoRepositoryImpl(
    private val remoteDs: PokemonApi,
    private val pokemonInfoMapper: Mapper<PokemonInfoResponse, PokemonInfoEntity>
) : PokemonInfoRepository {

    override suspend fun getPokemonInfo(pokemonName: String): PokemonInfoEntity {
        val pokemonInfo = remoteDs.getPokemonInfo(pokemonName)
        return pokemonInfoMapper.map(pokemonInfo)
    }
}

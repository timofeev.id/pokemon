package com.dmityinc.feature.pockemoninfo.presentation.composables


import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.height
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import com.dmityinc.design.theme.dp8

@Composable
internal fun TitleValue(
    title: String,
    value: String
) {
    Column(
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        Text(
            text = value,
            color = MaterialTheme.colors.surface,
            style = MaterialTheme.typography.h6
        )
        Spacer(modifier = Modifier.height(dp8))
        Text(
            text = title.uppercase(),
            color = MaterialTheme.colors.surface,
            style = MaterialTheme.typography.body2
        )
    }
}
package com.dmityinc.feature.pockemoninfo.domain

import com.dmityinc.core.domain.mapper.ErrorMapper
import com.dmityinc.core.model.data.Response
import com.dmityinc.core.model.data.doWithCatching
import com.dmityinc.feature.pockemoninfo.model.domain.PokemonInfoEntity

class GetPokemonInfoInteractorImpl(
    private val pokemonInfoRepo: PokemonInfoRepository,
    private val errorMapper: ErrorMapper
) : GetPokemonInfoInteractor {

    override suspend fun getPokemonInfo(name: String): Response<PokemonInfoEntity> = doWithCatching(
        doWork = { pokemonInfoRepo.getPokemonInfo(name) },
        errorMapper = errorMapper::map
    )
}

package com.dmityinc.feature.pockemoninfo.data

import com.dmityinc.core.domain.mapper.Mapper
import com.dmityinc.core.utils.format
import com.dmityinc.feature.pockemoninfo.model.domain.PokemonInfoEntity
import com.dmityinc.feature.pokemoncommon.model.data.pokemoninfo.PokemonInfoResponse
import com.dmityinc.feature.pokemoncommon.model.domain.PokemonType
import kotlin.random.Random

class GetPokemonInfoMapper : Mapper<PokemonInfoResponse, PokemonInfoEntity> {

    override fun map(from: PokemonInfoResponse): PokemonInfoEntity {
        return PokemonInfoEntity(
            name = from
                .name
                .orEmpty()
                .replaceFirstChar { it.titlecase() },
            height = from.height?.toDouble().format().orEmpty(),
            weight = from.weight?.toDouble().format().orEmpty(),
            imgUrlList = listOfNotNull(
                from.sprites?.frontDefault ?: from.sprites?.frontFemale,
                from.sprites?.backDefault ?: from.sprites?.backFemale,
                from.sprites?.frontShiny ?: from.sprites?.frontShinyFemale,
                from.sprites?.backShiny ?: from.sprites?.backShinyFemale,
            ),
            genTypes = from.types
                ?.mapNotNull { it.type?.name }
                ?.map { PokemonType.getInfoByType(it) }
                .orEmpty(),
            // Random number because api do not provide this data, but my design have such fields
            hp = Random.nextDouble(50.0, 100.0),
            strength = Random.nextDouble(10.0, 100.0),
            agility = Random.nextDouble(1.0, 100.0),
            attack = Random.nextDouble(5.0, 100.0),
            isFavorite = false
        )
    }
}

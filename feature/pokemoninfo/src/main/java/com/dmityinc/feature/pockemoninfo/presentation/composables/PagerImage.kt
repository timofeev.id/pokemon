package com.dmityinc.feature.pockemoninfo.presentation.composables

import android.graphics.Bitmap
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.wrapContentSize
import androidx.compose.runtime.Composable
import androidx.compose.runtime.State
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.graphicsLayer
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.util.lerp
import coil.compose.AsyncImage
import coil.request.ImageRequest
import coil.size.Dimension
import coil.size.Size
import coil.transform.Transformation
import com.dmityinc.core.utils.addShadow
import com.dmityinc.design.R
import com.dmityinc.design.theme.dp32
import com.dmityinc.design.theme.dp320
import com.google.accompanist.pager.ExperimentalPagerApi
import com.google.accompanist.pager.HorizontalPager
import com.google.accompanist.pager.calculateCurrentOffsetForPage
import com.google.accompanist.pager.rememberPagerState
import kotlinx.coroutines.launch
import kotlin.math.absoluteValue

@OptIn(ExperimentalPagerApi::class)
@Composable
internal fun PagerImage(
    modifier: Modifier = Modifier,
    imgUrlList: List<String>,
    viewpagerPage: State<Int>
) {
    val pagerState = rememberPagerState()
    val scope = rememberCoroutineScope()
    remember(key1 = viewpagerPage.value) {
        scope.launch {
            pagerState.animateScrollToPage(viewpagerPage.value)
        }
    }
    HorizontalPager(
        count = imgUrlList.size,
        state = pagerState,
        contentPadding = PaddingValues(horizontal = dp32),
        modifier = modifier.wrapContentSize()
    ) { page ->
        AsyncImage(
            model = ImageRequest.Builder(LocalContext.current)
                .data(imgUrlList[page])
                .crossfade(true)
                .transformations(
                    object : Transformation {
                        override val cacheKey: String
                            get() = page.toString()

                        override suspend fun transform(input: Bitmap, size: Size): Bitmap {
                            return input.addShadow(
                                dstWidth = (size.width as Dimension.Pixels).px,
                                dstHeight = (size.height as Dimension.Pixels).px,
                            ) ?: input
                        }
                    }
                )
                .build(),
            error = painterResource(id = R.drawable.ic_baseline_no_photography_24),
            contentDescription = null,
            modifier = Modifier
                .size(dp320)
                .graphicsLayer {
                    val pageOffset = calculateCurrentOffsetForPage(page).absoluteValue
                    lerp(
                        start = 0.5f,
                        stop = 1f,
                        fraction = 1f - pageOffset.coerceIn(0f, 1f)
                    ).also { scale ->
                        scaleX = scale
                        scaleY = scale
                    }

                    alpha = lerp(
                        start = 0.5f,
                        stop = 1f,
                        fraction = 1f - pageOffset.coerceIn(0f, 1f)
                    )
                }
        )
    }
}

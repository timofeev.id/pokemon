package com.dmityinc.feature.pockemoninfo.di

import com.dmityinc.core.domain.mapper.ErrorMapper
import com.dmityinc.core.domain.mapper.Mapper
import com.dmityinc.feature.pockemoninfo.data.GetPokemonInfoMapper
import com.dmityinc.feature.pockemoninfo.data.PokemonInfoRepositoryImpl
import com.dmityinc.feature.pockemoninfo.domain.GetPokemonInfoInteractor
import com.dmityinc.feature.pockemoninfo.domain.GetPokemonInfoInteractorImpl
import com.dmityinc.feature.pockemoninfo.domain.PokemonInfoRepository
import com.dmityinc.feature.pockemoninfo.model.domain.PokemonInfoEntity
import com.dmityinc.feature.pokemoncommon.data.PokemonApi
import com.dmityinc.feature.pokemoncommon.model.data.pokemoninfo.PokemonInfoResponse
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ViewModelComponent

@InstallIn(ViewModelComponent::class)
@Module
object PokemonInfoModule {

    @Provides
    fun provideGetPokemonInfoMapper(): Mapper<PokemonInfoResponse, PokemonInfoEntity> {
        return GetPokemonInfoMapper()
    }

    @Provides
    fun providePokemonInfoRepository(
        remoteDs: PokemonApi,
        mapper: Mapper<PokemonInfoResponse, PokemonInfoEntity>
    ): PokemonInfoRepository {
        return PokemonInfoRepositoryImpl(
            remoteDs = remoteDs,
            pokemonInfoMapper = mapper
        )
    }

    @Provides
    fun provideGetPokemonUseCase(
        pokemonInfoRepository: PokemonInfoRepository,
        errorMapper: ErrorMapper
    ): GetPokemonInfoInteractor {
        return GetPokemonInfoInteractorImpl(
            pokemonInfoRepo = pokemonInfoRepository,
            errorMapper = errorMapper
        )
    }
}

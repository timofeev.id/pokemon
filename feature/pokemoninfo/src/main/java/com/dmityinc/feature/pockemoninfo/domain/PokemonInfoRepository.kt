package com.dmityinc.feature.pockemoninfo.domain

import com.dmityinc.feature.pockemoninfo.model.domain.PokemonInfoEntity

interface PokemonInfoRepository {

    suspend fun getPokemonInfo(pokemonName: String): PokemonInfoEntity
}

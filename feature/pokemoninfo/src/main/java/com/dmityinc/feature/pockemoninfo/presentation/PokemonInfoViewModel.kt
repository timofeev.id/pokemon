package com.dmityinc.feature.pockemoninfo.presentation;

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.dmityinc.core.model.data.doOnComplete
import com.dmityinc.core.model.data.doOnError
import com.dmityinc.core.model.data.doOnSuccess
import com.dmityinc.core.model.domain.ErrorEntity
import com.dmityinc.feature.pockemoninfo.R
import com.dmityinc.feature.pockemoninfo.domain.GetPokemonInfoInteractor
import com.dmityinc.feature.pockemoninfo.model.presentation.PokemonInfoViewState
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class PokemonInfoViewModel @Inject constructor(
    private val pokemonInfoInteractor: GetPokemonInfoInteractor
) : ViewModel() {

    val viewState: PokemonInfoViewState = PokemonInfoViewState()
    var pokemonName: String = ""
    private var scrollJob: Job? = null

    fun getPokemonInfo() = viewModelScope.launch(Dispatchers.IO) {
        if (pokemonName.isBlank()) {
            viewState.error.value = ErrorEntity.UiError(R.string.pokemon_not_define)
            return@launch
        }
        scrollJob?.cancel()
        viewState.isLoading.value = true
        pokemonInfoInteractor
            .getPokemonInfo(pokemonName)
            .doOnSuccess {
                viewState.data.value = it
                scrollPager(it.imgUrlList.size)
            }
            .doOnError {
                viewState.error.value = it
            }
            .doOnComplete {
                viewState.isLoading.value = false
            }
    }

    fun addToFavoriteClick() {
        // TODO Save to DB
        val data = viewState.data.value
        viewState.data.value = data?.copy(
            isFavorite = data.isFavorite.not()
        )
    }

    private fun scrollPager(numberOfPages: Int) {
        scrollJob = viewModelScope.launch(Dispatchers.IO) {
            var currPage = 0
            while (true) {
                delay(DELAY_PAGER_SCROLL)
                currPage++
                viewState.viewpagerPage.value = currPage % numberOfPages
            }
        }
    }

    private companion object {
        /** Delay between viewPager scroll */
        const val DELAY_PAGER_SCROLL = 5_000L
    }
}

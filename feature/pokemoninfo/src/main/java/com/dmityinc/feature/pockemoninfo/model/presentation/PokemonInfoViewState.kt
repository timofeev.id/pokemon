package com.dmityinc.feature.pockemoninfo.model.presentation

import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import com.dmityinc.core.model.presentation.BaseViewState
import com.dmityinc.feature.pockemoninfo.model.domain.PokemonInfoEntity

class PokemonInfoViewState(
    val data: MutableState<PokemonInfoEntity?> = mutableStateOf(null),
    val viewpagerPage: MutableState<Int> = mutableStateOf(0)
) : BaseViewState()

package com.dmityinc.feature.pockemoninfo.domain

import com.dmityinc.core.model.data.Response
import com.dmityinc.feature.pockemoninfo.model.domain.PokemonInfoEntity

interface GetPokemonInfoInteractor {

    suspend fun getPokemonInfo(name: String): Response<PokemonInfoEntity>
}

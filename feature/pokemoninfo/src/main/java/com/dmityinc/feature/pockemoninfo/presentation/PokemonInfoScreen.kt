package com.dmityinc.feature.pockemoninfo.presentation

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.State
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.navigation.NavHostController
import com.dmityinc.design.R
import com.dmityinc.design.composables.AppBar
import com.dmityinc.design.composables.FullScreenError
import com.dmityinc.design.composables.Loading
import com.dmityinc.design.composables.utils.OneTimeEffect
import com.dmityinc.design.composables.utils.SetStatusBarColor
import com.dmityinc.design.theme.Green300
import com.dmityinc.design.theme.Orange500
import com.dmityinc.design.theme.Yellow300
import com.dmityinc.design.theme.Yellow500
import com.dmityinc.design.theme.dp16
import com.dmityinc.design.theme.dp24
import com.dmityinc.design.theme.dp4
import com.dmityinc.design.theme.dp8
import com.dmityinc.design.theme.toBold
import com.dmityinc.feature.pockemoninfo.model.domain.PokemonInfoEntity
import com.dmityinc.feature.pockemoninfo.presentation.composables.AnimatedProgressBar
import com.dmityinc.feature.pockemoninfo.presentation.composables.PagerImage
import com.dmityinc.feature.pockemoninfo.presentation.composables.TitleValue
import com.dmityinc.feature.pockemoninfo.presentation.composables.TypeList
import com.dmityinc.feature.pockemoninfo.R as local

@Composable
fun PokemonInfoScreen(
    viewModel: PokemonInfoViewModel,
    navController: NavHostController,
    paddingValues: PaddingValues
) {
    SetStatusBarColor(isDark = false)
    OneTimeEffect { viewModel.getPokemonInfo() }
    val state = viewModel.viewState
    val data = state.data.value
    val messageToUserRes = state.error.value?.messageToUserRes
    when {
        data != null -> {
            Body(
                modifier = Modifier
                    .fillMaxSize()
                    .background(data.genTypes.first().color)
                    .padding(paddingValues),
                onFavoriteClick = viewModel::addToFavoriteClick,
                navController = navController,
                data = data,
                viewPagerPage = state.viewpagerPage
            )
        }
        state.isLoading.value -> {
            Loading(
                modifier = Modifier
                    .fillMaxSize()
                    .padding(paddingValues)
            )
        }
        messageToUserRes != null -> {
            FullScreenError(
                modifier = Modifier.padding(paddingValues),
                msg = stringResource(id = messageToUserRes),
                showRetryBtn = true,
                onRetryClick = viewModel::getPokemonInfo
            )
        }
    }
}

@Composable
private fun Body(
    modifier: Modifier,
    onFavoriteClick: () -> Unit,
    navController: NavHostController,
    data: PokemonInfoEntity,
    viewPagerPage: State<Int>
) {
    Column(
        modifier = modifier.fillMaxSize()
    ) {
        AppBar(
            menuIcon = if (data.isFavorite) R.drawable.ic_baseline_star else R.drawable.ic_baseline_star_border,
            onMenuClick = onFavoriteClick,
            onIconClick = navController::popBackStack
        )
        PagerImage(
            imgUrlList = data.imgUrlList,
            viewpagerPage = viewPagerPage
        )
        Text(
            text = data.name,
            style = MaterialTheme.typography.h2.toBold(),
            color = MaterialTheme.colors.background,
            textAlign = TextAlign.Center,
            modifier = Modifier.fillMaxWidth()
        )
        Spacer(modifier = Modifier.height(dp24))
        BlockIndo(data)
    }
}

@Composable
fun BlockIndo(data: PokemonInfoEntity) {
    Column(
        modifier = Modifier
            .padding(horizontal = dp16)
            .fillMaxWidth()
            .background(
                color = MaterialTheme
                    .colors
                    .background
                    .copy(alpha = 0.4f),
                shape = RoundedCornerShape(dp4)
            )
            .padding(dp16)
    ) {
        Row(
            modifier = Modifier.fillMaxWidth(),
            horizontalArrangement = Arrangement.SpaceAround
        ) {
            TitleValue(
                title = stringResource(id = local.string.weight),
                value = stringResource(id = local.string.weight_sign, data.weight.toString())
            )
            TypeList(data.genTypes)
            TitleValue(
                title = stringResource(id = local.string.height),
                value = stringResource(id = local.string.height_sign, data.height.toString())
            )
        }
        Spacer(modifier = Modifier.height(dp16))
        AnimatedProgressBar(
            activeColor = Green300,
            currentValue = data.hp,
            title = stringResource(id = local.string.hp)
        )
        Spacer(modifier = Modifier.height(dp8))
        AnimatedProgressBar(
            activeColor = Yellow500,
            currentValue = data.strength,
            title = stringResource(id = local.string.strength)
        )
        Spacer(modifier = Modifier.height(dp8))
        AnimatedProgressBar(
            activeColor = Yellow300,
            currentValue = data.agility,
            title = stringResource(id = local.string.agility)
        )
        Spacer(modifier = Modifier.height(dp8))
        AnimatedProgressBar(
            activeColor = Orange500,
            currentValue = data.attack,
            title = stringResource(id = local.string.attack)
        )
    }
}

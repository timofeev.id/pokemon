package com.dmityinc.feature.pockemonslist.data

import com.dmityinc.core.domain.mapper.Mapper
import com.dmityinc.feature.pockemonslist.domain.PokemonListRepository
import com.dmityinc.feature.pockemonslist.model.domain.PokemonItemListEntity
import com.dmityinc.feature.pokemoncommon.data.PokemonApi
import com.dmityinc.feature.pokemoncommon.model.data.PokemonFormResponse

class PokemonListRepositoryImpl(
    private val remoteDs: PokemonApi,
    // TODO Add local DS ROOM for cache
    private val pokemonsMapper: Mapper<PokemonFormResponse, PokemonItemListEntity>
) : PokemonListRepository {

    override suspend fun getPokemonNames(offset: Int, limit: Int): List<String> {
        return remoteDs
            .getPokemonList(
                limit = limit,
                offset = offset
            )
            .results
            ?.mapNotNull { it.name }
            .orEmpty()
    }

    override suspend fun getPokemonShortInfo(pokemonName: String): PokemonItemListEntity {
        val pokemonShortInfo = remoteDs.getPokemonForm(pokemonName)
        return pokemonsMapper.map(pokemonShortInfo)
    }
}

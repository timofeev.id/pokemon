package com.dmityinc.feature.pockemonslist.di

import com.dmityinc.core.domain.mapper.ErrorMapper
import com.dmityinc.core.domain.mapper.Mapper
import com.dmityinc.feature.pockemonslist.data.GetPokemonsMapper
import com.dmityinc.feature.pockemonslist.data.PokemonListRepositoryImpl
import com.dmityinc.feature.pockemonslist.domain.GetPokemonsInteractor
import com.dmityinc.feature.pockemonslist.domain.GetPokemonsInteractorImpl
import com.dmityinc.feature.pockemonslist.domain.PokemonListRepository
import com.dmityinc.feature.pockemonslist.model.domain.PokemonItemListEntity
import com.dmityinc.feature.pokemoncommon.data.PokemonApi
import com.dmityinc.feature.pokemoncommon.model.data.PokemonFormResponse
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ViewModelComponent

@InstallIn(ViewModelComponent::class)
@Module
object PokemonListModule {

    @Provides
    fun provideGetPokemonMapper(): Mapper<PokemonFormResponse, PokemonItemListEntity> {
        return GetPokemonsMapper()
    }

    @Provides
    fun providePokemonListRepository(
        remoteDs: PokemonApi,
        mapper: Mapper<PokemonFormResponse, PokemonItemListEntity>
    ): PokemonListRepository {
        return PokemonListRepositoryImpl(
            remoteDs = remoteDs,
            pokemonsMapper = mapper
        )
    }

    @Provides
    fun provideGetPokemonUseCase(
        pokemonListRepository: PokemonListRepository,
        errorMapper: ErrorMapper
    ): GetPokemonsInteractor {
        return GetPokemonsInteractorImpl(
            pokemonListRepo = pokemonListRepository,
            errorMapper = errorMapper
        )
    }
}

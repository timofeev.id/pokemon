package com.dmityinc.feature.pockemonslist.presentation

import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.grid.GridCells
import androidx.compose.foundation.lazy.grid.GridItemSpan
import androidx.compose.foundation.lazy.grid.LazyVerticalGrid
import androidx.compose.foundation.lazy.grid.items
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.navigation.NavHostController
import com.dmityinc.design.composables.FullScreenError
import com.dmityinc.design.composables.Loading
import com.dmityinc.design.composables.utils.OneTimeEffect
import com.dmityinc.design.composables.utils.SetStatusBarColor
import com.dmityinc.design.theme.dp16
import com.dmityinc.design.theme.dp8
import com.dmityinc.feature.pockemonslist.R
import com.dmityinc.feature.pockemonslist.presentation.composables.PokemonCard

@Composable
fun PokemonListScreen(
    viewModel: PokemonListViewModel,
    navController: NavHostController,
    paddingValues: PaddingValues
) {
    SetStatusBarColor(isDark = true)
    OneTimeEffect { viewModel.getPokemonList() }
    val state = viewModel.viewState
    val messageToUserRes = state.error.value?.messageToUserRes
    when {
        messageToUserRes != null -> {
            FullScreenError(
                modifier = Modifier.padding(paddingValues),
                msg = stringResource(id = messageToUserRes),
                showRetryBtn = true,
                onRetryClick = viewModel::getPokemonList
            )
        }
        state.pokemonList.isNullOrEmpty().not() -> {
            PokemonCardList(
                viewModel = viewModel,
                navController = navController,
                paddingValues = paddingValues
            )
        }
        state.pokemonList.isEmpty() &&
                state.isLoading.value.not() -> {
            FullScreenError(
                modifier = Modifier.padding(paddingValues),
                msg = stringResource(id = R.string.pokemon_not_found_msg),
                showRetryBtn = true,
                onRetryClick = viewModel::getPokemonList
            )
        }
        state.isLoading.value -> {
            Loading(
                modifier = Modifier
                    .fillMaxSize()
                    .padding(paddingValues)
            )
        }
    }
}

@Composable
private fun PokemonCardList(
    viewModel: PokemonListViewModel,
    navController: NavHostController,
    paddingValues: PaddingValues
) {
    LazyVerticalGrid(
        modifier = Modifier
            .fillMaxSize()
            .padding(horizontal = dp8),
        contentPadding = paddingValues,
        columns = GridCells.Fixed(2),
    ) {
        item(
            span = { GridItemSpan(2) }
        ) {
            Text(
                modifier = Modifier.padding(dp16),
                text = stringResource(id = R.string.pokedex),
                style = MaterialTheme.typography.h5
            )
        }

        items(
            items = viewModel.viewState.pokemonList,
            key = { item -> item.name },
            itemContent = { item ->
                PokemonCard(
                    pokemon = item,
                    onPokemonClick = {
                        val infoScreen = viewModel.getPokemonScreen(it)
                        navController.navigate(infoScreen)
                    }
                )
            }
        )
    }
}

package com.dmityinc.feature.pockemonslist.model.presentation

class GetPokemonsUseCaseParams(
    val limit: Int,
    val offset: Int
)
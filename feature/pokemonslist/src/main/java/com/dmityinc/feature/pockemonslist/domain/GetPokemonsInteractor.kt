package com.dmityinc.feature.pockemonslist.domain

import com.dmityinc.core.model.data.Response
import com.dmityinc.feature.pockemonslist.model.domain.PokemonItemListEntity
import com.dmityinc.feature.pockemonslist.model.presentation.GetPokemonsUseCaseParams
import kotlinx.coroutines.flow.Flow

interface GetPokemonsInteractor {

    fun getPokemonsShortInfo(data: GetPokemonsUseCaseParams): Flow<Response<PokemonItemListEntity>>
}

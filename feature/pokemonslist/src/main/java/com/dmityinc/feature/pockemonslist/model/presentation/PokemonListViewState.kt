package com.dmityinc.feature.pockemonslist.model.presentation

import androidx.compose.runtime.mutableStateListOf
import androidx.compose.runtime.snapshots.SnapshotStateList
import com.dmityinc.core.model.presentation.BaseViewState
import com.dmityinc.feature.pockemonslist.model.domain.PokemonItemListEntity

class PokemonListViewState(
    val pokemonList: SnapshotStateList<PokemonItemListEntity> = mutableStateListOf()
) : BaseViewState()

package com.dmityinc.feature.pockemonslist.presentation.composables

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.shape.CutCornerShape
import androidx.compose.material.ExperimentalMaterialApi
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Alignment.Companion.CenterHorizontally
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import coil.compose.AsyncImage
import coil.request.ImageRequest
import com.dmityinc.core.utils.radialGradient
import com.dmityinc.design.theme.dp12
import com.dmityinc.design.theme.dp128
import com.dmityinc.design.theme.dp160
import com.dmityinc.design.theme.dp4
import com.dmityinc.design.theme.dp8
import com.dmityinc.design.theme.toBold
import com.dmityinc.feature.pockemonslist.R
import com.dmityinc.feature.pockemonslist.model.domain.PokemonItemListEntity
import com.dmityinc.design.R as design

@OptIn(ExperimentalMaterialApi::class)
@Composable
internal fun PokemonCard(
    pokemon: PokemonItemListEntity,
    onPokemonClick: (pokemonName: String) -> Unit
) {
    Surface(
        modifier = Modifier.padding(dp4),
        onClick = { onPokemonClick.invoke(pokemon.name) },
        shape = CutCornerShape(dp12),
        elevation = dp4
    ) {
        Column(
            modifier = Modifier
                .radialGradient(listOf(Color.White, pokemon.bgColor))
                .padding(dp8),
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            AsyncImage(
                model = ImageRequest.Builder(LocalContext.current)
                    .data(pokemon.frontDefault)
                    .crossfade(true)
                    .build(),
                error = painterResource(id = design.drawable.ic_baseline_no_photography_24),
                contentDescription = stringResource(id = R.string.pokemon_icon_desc),
                contentScale = ContentScale.FillWidth,
                modifier = Modifier
                    .width(dp160)
                    .height(dp128)
            )
            Spacer(modifier = Modifier.height(dp8))
            Text(
                modifier = Modifier.align(CenterHorizontally),
                text = pokemon.name,
                color = Color.White,
                style = MaterialTheme.typography.body1.toBold()
            )
        }
    }
}

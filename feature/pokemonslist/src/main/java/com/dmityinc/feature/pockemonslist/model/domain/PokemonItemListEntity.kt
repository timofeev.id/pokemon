package com.dmityinc.feature.pockemonslist.model.domain

import androidx.compose.ui.graphics.Color

data class PokemonItemListEntity (
    val name: String,
    val frontDefault: String?,
    val bgColor: Color,
    val type: String
)

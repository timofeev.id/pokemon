package com.dmityinc.feature.pockemonslist.domain

import com.dmityinc.core.domain.mapper.ErrorMapper
import com.dmityinc.core.model.data.doWithCatching
import com.dmityinc.feature.pockemonslist.model.presentation.GetPokemonsUseCaseParams
import kotlinx.coroutines.flow.flow
import timber.log.Timber

class GetPokemonsInteractorImpl(
    private val pokemonListRepo: PokemonListRepository,
    private val errorMapper: ErrorMapper
) : GetPokemonsInteractor {

    override fun getPokemonsShortInfo(data: GetPokemonsUseCaseParams) = flow {
        getPokemonItemList(data)
            .forEach { name ->
                val pokemonInfo = doWithCatching(
                    doWork = { pokemonListRepo.getPokemonShortInfo(name) },
                    errorMapper = errorMapper::map
                )
                emit(pokemonInfo)
            }
    }

    private suspend fun getPokemonItemList(data: GetPokemonsUseCaseParams): List<String> {
        return try {
            pokemonListRepo.getPokemonNames(
                offset = data.offset,
                limit = data.limit
            )
        } catch (e: Exception) {
            Timber.d(e, "Error during getPokemonNames")
            emptyList()
        }
    }
}

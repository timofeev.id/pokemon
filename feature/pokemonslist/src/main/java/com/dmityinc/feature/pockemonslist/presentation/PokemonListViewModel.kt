package com.dmityinc.feature.pockemonslist.presentation;

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.dmityinc.core.model.data.Response
import com.dmityinc.core.model.data.doOnComplete
import com.dmityinc.core.model.data.doOnError
import com.dmityinc.core.model.data.doOnSuccess
import com.dmityinc.feature.pockemonslist.domain.GetPokemonsInteractor
import com.dmityinc.feature.pockemonslist.model.domain.PokemonItemListEntity
import com.dmityinc.feature.pockemonslist.model.presentation.GetPokemonsUseCaseParams
import com.dmityinc.feature.pockemonslist.model.presentation.PokemonListViewState
import com.dmityinc.navigation.api.NavigationRoutes.PockemonFeature.PokemonInfo.pokemonInfoScreenLink
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch
import timber.log.Timber
import javax.inject.Inject

@HiltViewModel
class PokemonListViewModel @Inject constructor(
    private val getPokemonsInteractor: GetPokemonsInteractor
) : ViewModel() {

    val viewState = PokemonListViewState()

    fun getPokemonScreen(pokemonName: String): String {
        Timber.d("On pokemon click $pokemonName")
        return pokemonInfoScreenLink(pokemonName.lowercase())
    }

    fun getPokemonList() = viewModelScope.launch(Dispatchers.IO) {
        viewState.isLoading.value = true
        val request = GetPokemonsUseCaseParams(limit = 10, offset = 0)
        getPokemonsInteractor
            .getPokemonsShortInfo(request)
            .onEach(::handlePokemonItemResponse)
            .flowOn(Dispatchers.IO)
            .collect()
    }

    private fun handlePokemonItemResponse(response: Response<PokemonItemListEntity>) {
        response
            .doOnSuccess {
                viewState.error.value = null
                if (viewState.pokemonList.contains(it).not()) {
                    viewState.pokemonList.add(it)
                }
            }
            .doOnError {
                viewState.error.value = it
            }
            .doOnComplete {
                if (viewState.isLoading.value) {
                    viewState.isLoading.value = false
                }
            }
    }
}

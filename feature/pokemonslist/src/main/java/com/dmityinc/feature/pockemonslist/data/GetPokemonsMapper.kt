package com.dmityinc.feature.pockemonslist.data

import com.dmityinc.core.domain.mapper.Mapper
import com.dmityinc.feature.pockemonslist.model.domain.PokemonItemListEntity
import com.dmityinc.feature.pokemoncommon.model.data.PokemonFormResponse
import com.dmityinc.feature.pokemoncommon.model.domain.PokemonType

class GetPokemonsMapper : Mapper<PokemonFormResponse, PokemonItemListEntity> {

    override fun map(from: PokemonFormResponse): PokemonItemListEntity {
        val type = from.types?.lastOrNull()?.type?.name.orEmpty()
        return PokemonItemListEntity(
            name = from
                .name
                .orEmpty()
                .replaceFirstChar { it.titlecase() },
            frontDefault = from.sprites?.frontDefault,
            bgColor = PokemonType.getInfoByType(type).color,
            type = type
        )
    }
}

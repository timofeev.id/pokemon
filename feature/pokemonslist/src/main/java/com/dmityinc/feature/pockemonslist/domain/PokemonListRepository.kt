package com.dmityinc.feature.pockemonslist.domain

import com.dmityinc.feature.pockemonslist.model.domain.PokemonItemListEntity

interface PokemonListRepository {

    suspend fun getPokemonNames(
        offset: Int = 0,
        limit: Int = 10
    ): List<String>

    suspend fun getPokemonShortInfo(pokemonName: String): PokemonItemListEntity
}

plugins {
    id("com.android.library")
    kotlin("android")
    kotlin("kapt")
    id("dagger.hilt.android.plugin")
}

apply {
    from("$rootDir/config/common-feature-module.gradle")
    from("$rootDir/config/compose-feature-module.gradle")
    from("$rootDir/config/common-hilt.gradle")
}

dependencies {
    implementation(project(":core"))
    implementation(project(":design"))
    implementation(project(":navigation:api"))
    implementation(project(":feature:pokemoncommon"))
    implementation(libs.androidx.viewmodel)
    implementation(libs.coil)
}
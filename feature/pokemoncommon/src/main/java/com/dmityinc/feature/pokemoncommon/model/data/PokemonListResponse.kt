package com.dmityinc.feature.pokemoncommon.model.data

import com.google.gson.annotations.SerializedName

class PokemonListResponse(
    @SerializedName("count")
    val count: Int?,
    @SerializedName("next")
    val next: String?,
    @SerializedName("previous")
    val previous: Any?,
    @SerializedName("results")
    val results: List<Result>?
)

class Result(
    @SerializedName("name")
    val name: String?,
    @SerializedName("url")
    val url: String?
)
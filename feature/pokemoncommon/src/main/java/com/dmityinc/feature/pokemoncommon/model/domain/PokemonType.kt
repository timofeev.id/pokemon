package com.dmityinc.feature.pokemoncommon.model.domain

import androidx.compose.ui.graphics.Color

enum class PokemonType(
    val type: String,
    val color: Color
) {
    FIGHTING("fighting", fighting),
    FLYING("flying", flying),
    POISON("poison", poison),
    GROUND("ground", ground),
    ROCK("rock", rock),
    BUG("bug", bug),
    GHOST("ghost", ghost),
    STEEL("steel", steel),
    FIRE("fire", fire),
    WATER("water", water),
    GRASS("grass", grass),
    ELECTRIC("electric", electric),
    PSYCHIC("psychic", psychic),
    ICE("ice", ice),
    DRAGON("dragon", dragon),
    FAIRY("fairy", fairy),
    DARK("dark", dark),
    NORMAL("normal", normal),
    UNKNOWN("unknown", unknown);

    companion object {
        fun getInfoByType(type: String) = values().firstOrNull { it.type == type } ?: UNKNOWN
    }
}

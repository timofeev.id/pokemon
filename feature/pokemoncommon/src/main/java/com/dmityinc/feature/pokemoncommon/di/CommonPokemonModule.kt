package com.dmityinc.feature.pokemoncommon.di

import com.dmityinc.core.data.network.ApiClient
import com.dmityinc.core.domain.mapper.ErrorMapper
import com.dmityinc.feature.pokemoncommon.data.PokemonApi
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object CommonPokemonModule {

    @Provides
    @Singleton
    fun providePokemonApiService(): PokemonApi {
        return ApiClient.createApiClient(
            classT = PokemonApi::class.java,
            apiUrl = PokemonApi.BASE_URL
        )
    }

    @Provides
    fun provideErrorMapper(): ErrorMapper = ErrorMapper(ApiClient.gson)

}
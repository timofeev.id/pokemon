package com.dmityinc.feature.pokemoncommon.model.data.pokemoninfo

import com.google.gson.annotations.SerializedName

class Stat(
    @SerializedName("base_stat")
    val baseStat: Int?,
    @SerializedName("effort")
    val effort: Int?,
    @SerializedName("stat")
    val stat: StatX?
)

class StatX(
    @SerializedName("name")
    val name: String?,
    @SerializedName("url")
    val url: String?
)
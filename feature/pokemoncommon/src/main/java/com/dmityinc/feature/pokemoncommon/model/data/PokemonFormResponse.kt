package com.dmityinc.feature.pokemoncommon.model.data

import com.dmityinc.feature.pokemoncommon.model.data.pokemoninfo.Sprites
import com.dmityinc.feature.pokemoncommon.model.data.pokemoninfo.Type
import com.google.gson.annotations.SerializedName

class PokemonFormResponse(
    @SerializedName("form_name")
    val formName: String?,
    @SerializedName("form_names")
    val formNames: List<Any>?,
    @SerializedName("form_order")
    val formOrder: Int?,
    @SerializedName("id")
    val id: Int?,
    @SerializedName("is_battle_only")
    val isBattleOnly: Boolean?,
    @SerializedName("is_default")
    val isDefault: Boolean?,
    @SerializedName("is_mega")
    val isMega: Boolean?,
    @SerializedName("name")
    val name: String?,
    @SerializedName("names")
    val names: List<Any>?,
    @SerializedName("order")
    val order: Int?,
    @SerializedName("pokemon")
    val pokemon: Pokemon?,
    @SerializedName("sprites")
    val sprites: Sprites?,
    @SerializedName("types")
    val types: List<Type>?
)

class Pokemon(
    @SerializedName("name")
    val name: String?,
    @SerializedName("url")
    val url: String?
)
package com.dmityinc.feature.pokemoncommon.model.data.pokemoninfo

import com.google.gson.annotations.SerializedName

class GameIndice(
    @SerializedName("game_index")
    val gameIndex: Int?,
    @SerializedName("version")
    val version: Version?
)

class Version(
    @SerializedName("name")
    val name: String?,
    @SerializedName("url")
    val url: String?
)
package com.dmityinc.feature.pokemoncommon.model.data.pokemoninfo.generation

import com.google.gson.annotations.SerializedName

class GenerationI(
    @SerializedName("red-blue")
    val redBlue: RedBlue?,
    @SerializedName("yellow")
    val yellow: Yellow?
)

class RedBlue(
    @SerializedName("back_default")
    val backDefault: String?,
    @SerializedName("back_gray")
    val backGray: String?,
    @SerializedName("back_transparent")
    val backTransparent: String?,
    @SerializedName("front_default")
    val frontDefault: String?,
    @SerializedName("front_gray")
    val frontGray: String?,
    @SerializedName("front_transparent")
    val frontTransparent: String?
)

class Yellow(
    @SerializedName("back_default")
    val backDefault: String?,
    @SerializedName("back_gray")
    val backGray: String?,
    @SerializedName("back_transparent")
    val backTransparent: String?,
    @SerializedName("front_default")
    val frontDefault: String?,
    @SerializedName("front_gray")
    val frontGray: String?,
    @SerializedName("front_transparent")
    val frontTransparent: String?
)
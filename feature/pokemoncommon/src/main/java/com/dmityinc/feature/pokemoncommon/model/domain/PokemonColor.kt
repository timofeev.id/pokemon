package com.dmityinc.feature.pokemoncommon.model.domain

import androidx.compose.ui.graphics.Color

val fighting = Color(0xFF9F2034)
val flying = Color(0xFF5B92B5)
val poison = Color(0xFF486C1E)
val ground = Color(0xFF916538)
val rock = Color(0xFF4A3B37)
val bug = Color(0xFF179A55)
val ghost = Color(0xDC625B9F)
val steel = Color(0xFF99AEA7)
val fire = Color(0xFFC62828)
val water = Color(0xFF307CFF)
val grass = Color(0xFF007C42)
val electric = Color(0xFFE6A84B)
val psychic = Color(0xFFC52F5D)
val ice = Color(0xFF7ECFF2)
val dragon = Color(0xFFC8070E)
val fairy = Color(0xFFB39DDB)
val dark = Color(0xFF535252)
val normal = Color(0xFF959595)
val unknown = Color(0xFFFFE082)

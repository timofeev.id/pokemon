package com.dmityinc.feature.pokemoncommon.model.data.pokemoninfo

import com.dmityinc.feature.pokemoncommon.model.data.pokemoninfo.generation.*
import com.google.gson.annotations.SerializedName

class Versions(
    @SerializedName("generation-i")
    val generationI: GenerationI?,
    @SerializedName("generation-ii")
    val generationIi: GenerationIi?,
    @SerializedName("generation-iii")
    val generationIii: GenerationIii?,
    @SerializedName("generation-iv")
    val generationIv: GenerationIv?,
    @SerializedName("generation-v")
    val generationV: GenerationV?,
    @SerializedName("generation-vi")
    val generationVi: GenerationVi?,
    @SerializedName("generation-vii")
    val generationVii: GenerationVii?,
    @SerializedName("generation-viii")
    val generationViii: GenerationViii?
)
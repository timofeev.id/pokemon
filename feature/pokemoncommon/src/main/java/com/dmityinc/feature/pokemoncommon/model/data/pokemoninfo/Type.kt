package com.dmityinc.feature.pokemoncommon.model.data.pokemoninfo

import com.google.gson.annotations.SerializedName

class Type(
    @SerializedName("slot")
    val slot: Int?,
    @SerializedName("type")
    val type: TypeX?
)

class TypeX(
    @SerializedName("name")
    val name: String?,
    @SerializedName("url")
    val url: String?
)
package com.dmityinc.feature.pokemoncommon.model.data.pokemoninfo

import com.google.gson.annotations.SerializedName

class HeldItem(
    @SerializedName("item")
    val item: Item?,
    @SerializedName("version_details")
    val versionDetails: List<VersionDetail>?
)

class Item(
    @SerializedName("name")
    val name: String?,
    @SerializedName("url")
    val url: String?
)

class VersionDetail(
    @SerializedName("rarity")
    val rarity: Int?,
    @SerializedName("version")
    val version: VersionX?
)

class VersionX(
    @SerializedName("name")
    val name: String?,
    @SerializedName("url")
    val url: String?
)
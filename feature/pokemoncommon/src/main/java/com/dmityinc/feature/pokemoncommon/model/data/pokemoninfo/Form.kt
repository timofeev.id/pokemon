package com.dmityinc.feature.pokemoncommon.model.data.pokemoninfo


import com.google.gson.annotations.SerializedName

class Form(
    @SerializedName("name")
    val name: String?,
    @SerializedName("url")
    val url: String?
)
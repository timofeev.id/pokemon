package com.dmityinc.feature.pokemoncommon.data

import com.dmityinc.feature.pokemoncommon.model.data.PokemonFormResponse
import com.dmityinc.feature.pokemoncommon.model.data.PokemonListResponse
import com.dmityinc.feature.pokemoncommon.model.data.pokemoninfo.PokemonInfoResponse
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface PokemonApi {

    companion object {
        const val BASE_URL: String = "https://pokeapi.co/api/v2/"
    }

    @GET("pokemon/{name}")
    suspend fun getPokemonInfo(@Path("name") name: String): PokemonInfoResponse

    @GET("pokemon-form/{name}")
    suspend fun getPokemonForm(@Path("name") name: String): PokemonFormResponse

    @GET("pokemon")
    suspend fun getPokemonList(
        @Query("limit") limit: Int,
        @Query("offset") offset: Int
    ): PokemonListResponse
}
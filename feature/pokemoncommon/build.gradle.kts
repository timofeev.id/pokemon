plugins {
    id("com.android.library")
    kotlin("android")
    kotlin("kapt")
    id("dagger.hilt.android.plugin")
}

apply {
    from("$rootDir/config/common-feature-module.gradle")
    from("$rootDir/config/common-hilt.gradle")
}

dependencies {
    implementation(project(":core"))
    implementation(libs.bundles.retrofit)
    implementation(libs.bundles.coroutines)
}
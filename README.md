# Pokemon app by Dmitry Timofeev

## 🎨 About

This project is based on PokeApi. I try to show my way of doing MVVM pattern with large scale multi
module project. Of course i use pattern with cut corners way, to minimize boilerplate code and
simplify project. As example mappers to presentation layer in most cases skipped because of no need to convert ready to UI model.

### 🗒 Overview

- MVVM architecture
  1. **data**: It contains all the data accessing and manipulating components.
  2. **domain**: Business logic, interactors, usecases.
  3. **presentation**: UI logic like ViewModel, compose UI
  4. **di**: Dagger files
  4. **model**: Classes with data itsels
  5. **utils**: Utility classes.
- Single activity, no fragment
- Feature based module structure

### 📱 Android SDK Components

- AndroidX
- Compose
- ViewModel
- Hilt
- Room

### 🔌 Other Third Party Libraries

- [PokeApi](https://pokeapi.co/)
- [Coil](https://coil-kt.github.io/coil/)
- [Coroutines](https://kotlinlang.org/docs/coroutines-overview.html)
- [Accompanist](https://github.com/google/accompanist)
- [Timber](https://github.com/JakeWharton/timber)

## ✅ TODO

- [X] Pokemons api
- [ ] Pagination pokemon list
- [ ] Favorite
- [ ] Settings
- [ ] Tests
- [ ] Documentation

## 🏛 License

```
Copyright 2022 Dmitry Timofeev

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
```

dependencyResolutionManagement {
    versionCatalogs {
        create("libs") {
            from(files("./config/libs.versions.toml"))
        }
    }
    repositories {
        google()
        mavenCentral()
    }
}

pluginManagement {
    val kotlinVersion = "1.6.10"
    val agpVersion = "7.1.3"

    plugins {
        kotlin("jvm") version kotlinVersion apply false
        kotlin("android") version kotlinVersion apply false
        id("com.android.application") version agpVersion apply false
        id("com.android.library") version agpVersion apply false
    }

    repositories {
        gradlePluginPortal()
        google()
        mavenCentral()
    }
}

include(
    ":app",
    ":core",
    ":design",
    ":feature:pokemoncommon",
    ":feature:pokemonslist",
    ":feature:pokemoninfo",
    ":navigation:impl",
    ":navigation:api"
)